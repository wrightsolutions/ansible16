#!/usr/bin/env python
#   Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

import re
from sys import argv,exit
from os import path as ospath
from os import stat

RE_COMMA_ENDING_LOOSE = re.compile(r',\s*\Z')
RE_COMMA_ENDING = re.compile(r',\Z')

def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not stat(target_path).st_size:
        return False
    return True


def print_comma_removed(filepath):

    stored_stored_line = None
    stored_line = None

    with open(ospath.expanduser(filepath)) as fin:
        for line in fin.readlines():
            if len(line.strip()) < 1:
                continue
            if stored_stored_line is not None:
                print stored_stored_line.rstrip()
            stored_stored_line = stored_line
            stored_line = line

    if stored_stored_line is not None:
        if RE_COMMA_ENDING_LOOSE.search(stored_stored_line.strip()):
            """ In terms of non-blank lines, if we detect that the second
            to last ends in comma, then we remove the comma
            """
            re_after = RE_COMMA_ENDING_LOOSE.subn('',stored_stored_line,1)
            if re_after[1] > 0:
                print re_after[0]
            else:
                print stored_stored_line
            #print stored_stored_line.replace(chr(44),'')
        else:
            print stored_stored_line.rstrip()
    if stored_line is not None:
        print stored_line

if __name__ == "__main__":
    if (len(argv) < 2):
        exit(101)
    print_comma_removed(argv[1])
