#!/usr/bin/awk
# Assist in manual process of data export mysql and import postgresql
# 'warning' in filename as a reminder that before running the script
# on the server you should have as much free disk space to store a
# new (extra) copy of the dump
# Reason is that we will write the insert lines to inserts.mysql file in /tmp/
#
# If you have multiple tables in a dump, then we are not going to separate out
# the inserts into individual files and leave it up to you
# to post process inserts if you want them separate
#
BEGIN { inscount=0;}
/^INSERT INTO/ { 
    inscount++;
    print $0 > "/tmp/inserts.mysql";
}
/^CREATE TABLE/,/^)/ { 
    if ($1 == "CREATE") { tbname=$3; }
    if ( $0 ~ /PRIMARY KEY/ ) {
	#sub(",$","",$0);
	print $0 > "/tmp/create_"tbname".mysql";
    } else if ( $0 ~ /UNIQUE KEY/ ) {
	sub(",$","",$4);
	print "CREATE UNIQUE INDEX "tbname"_idx_"$3" ON "tbname" "$4";" > "/tmp/constraint_"tbname".mysql";
    } else if ( $0 ~ /CONSTRAINT / ) {
	sub(",$","",$0);
	print $0";" > "/tmp/constraint_"tbname".mysql";
    } else if ( $0 ~ /KEY / ) {
	sub(",$","",$3);
	print "CREATE INDEX "tbname"_idx_"$2" ON "tbname" "$3";" > "/tmp/constraint_"tbname".mysql";
    } else {
	print $0 > "/tmp/create_"tbname".mysql";
    }
}
END { print "Counted =",inscount," INSERT INTO statements";}

