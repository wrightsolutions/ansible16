#!/usr/bin/awk
# Assist in manual process of data export mysql and import postgresql
BEGIN { inscount=0;}
/^INSERT INTO/ { inscount++; }
/^CREATE TABLE/,/^)/ { 
    if ($1 == "CREATE") { tbname=$3; }
    if ( $0 ~ /UNIQUE KEY/ ) {
	print "CREATE UNIQUE INDEX "tbname"_idx_"$3" ON "tbname" "$4";" > "/tmp/constraint_"tbname".mysql";
    } else { 
	print $0 > "/tmp/create_"tbname".mysql";
    }
}
END { print "Counted =",inscount," INSERT INTO statements";}
