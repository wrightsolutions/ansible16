# Typical playbook commands - Postgresql

~~~
ansible-playbook books/site.yml -i ~/hosts.list -u root -l mycent6 --tags "planner,postgres8,pg8bench"
~~~

## Debug or detailed test

~~~
ansible-playbook books/site.yml -i ~/hosts.list -u root -l mycent6 --tags "planner,postgres8,pg8bench" -vvv
~~~
