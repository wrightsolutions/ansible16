# Ansible 1.6 playbooks for CentOS 6

Updates required to playbooks imported from older Ansible as detailed
in the following two sections:

For action directive:
   name={{item}}   in place of older   name=$item

ERROR: only_if is not a legal parameter in an Ansible task or handler

Before: only_if: "is_set('${ansible_default_ipv4.address}')"
Suggestion for newer Ansible is shown next:
 when: ansible_default_ipv4.address is defined
