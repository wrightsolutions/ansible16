# Static conf examples

In here are working examples of master.cf frozen
from various stages of prototyping.

Those and other frozen configuration files
are retained for reference, rather than necessity,
as automation will build master.cf and
other configuration files on the fly.
