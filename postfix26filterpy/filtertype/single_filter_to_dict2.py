#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Gary Wright wrightsolutions.co.uk/contact
#
# Permission is hereby granted, free of charge,
# to any person obtaining a copy of this software and
# associated documentation files (the "Software"),
# to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from xml.etree import ElementTree as ET
tree = ET.parse('/tmp/mailFilters.xml')
root = tree.getroot()
atom_entries = root.findall("{http://www.w3.org/2005/Atom}entry")
atom_attributes = []

for item in atom_entries[0]:
    atom_attributes.append(item.attrib)
list_of_dicts = []

dict = {}
for item in atom_attributes:
    if 'name' in item:
        dkey = item['name']
        if 'value' in item:
            dval = item['value']
            dict[dkey] = dval

list_of_dicts.append(dict)

for idx,dict in enumerate(list_of_dicts):
    print "Filter {0} dictionary processed next:".format(idx)
    for k,v in dict.items():
        print k,v

