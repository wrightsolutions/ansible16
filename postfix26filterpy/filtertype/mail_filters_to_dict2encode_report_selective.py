#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Gary Wright wrightsolutions.co.uk/contact
#
# Permission is hereby granted, free of charge,
# to any person obtaining a copy of this software and
# associated documentation files (the "Software"),
# to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from xml.etree import ElementTree as ET
tree = ET.parse('/tmp/mailFilters.xml')
root = tree.getroot()
atom_entries = root.findall("{http://www.w3.org/2005/Atom}entry")

list_of_dicts = []
for entry in atom_entries:
    #print "Atom entry being processed."
    atom_dict = {}
    for elem in entry:
        if 'name' in elem.attrib:
            atom_attrib = elem.attrib
            dkey = atom_attrib['name']
            dvalue = None
            if 'value' in elem.attrib:
                dvalue = atom_attrib['value']
            if dvalue:
                atom_dict[dkey]=dvalue

    if len(atom_dict) > 0:
        list_of_dicts.append(atom_dict)


# Before writing scripts that handle unicode, it is
# worth having a quick analysis script that simply
# reports which item is at issue
print "Have {0} dictionaries in list to process".format(len(list_of_dicts))
for idx,dict in enumerate(list_of_dicts):
    print "### Filter {0} dictionary processed next:".format(idx)
    for dkey,dval in dict.items():
        if dkey not in ['doesNotHaveTheWord','hasTheWord','subject']:
            """ Be selective about key - we are only interested in three items
            at this stage - subject and hasTheWord and doesNotHaveTheWord
            """
            continue
        try:
            #kstr = str(k)
            valencoded = dval.encode('ascii')
            print "{0}: {1}".format(dkey,dval)
        except UnicodeEncodeError:
            print "### Filter {0} {1} {2}: {3}".format(
                idx,'+'*6,dkey.encode('ascii'),dval.encode('utf-8'))


