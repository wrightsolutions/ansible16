#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Gary Wright wrightsolutions.co.uk/contact
#
# Permission is hereby granted, free of charge,
# to any person obtaining a copy of this software and
# associated documentation files (the "Software"),
# to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from xml.etree import ElementTree as ET
from os import linesep as oslinesep
from sys import exit
try:
    import yaml
except ImportError,e:
    print "Is python-yaml / PyYAML installed? Import error {0}".format(e)
    # apt-get install python-yaml OR yum search PyYAML
    exit(101)

YINDENT=2
# Set YINDENT to 4 or some other value if you have a strong preference
""" OSLINESEP=oslinesep
Although we include the facility this will be a local decision
for the user / implementer and no pull requests or issues will
be prioritised for scripts used in any environment
other than the target server. Uncomment or change if vital to local use.
"""
OSLINESEP='\n'

tree = ET.parse('/tmp/mailFilters.xml')
root = tree.getroot()
atom_entries = root.findall("{http://www.w3.org/2005/Atom}entry")

KEY_DEFAULT_ENCODE='ascii'
VALUE_DEFAULT_ENCODE='utf-8'

list_of_dicts = []
for entry in atom_entries:
    #print "Atom entry being processed."
    atom_dict = {}
    for elem in entry:
        if 'name' in elem.attrib:
            atom_attrib = elem.attrib
            dkey = atom_attrib['name']
            dvalue = None
            if 'value' in elem.attrib:
                dvalue = atom_attrib['value']
            if dvalue:
                atom_dict[dkey]=dvalue

    if len(atom_dict) > 0:
        list_of_dicts.append(atom_dict)


print "Have {0} dictionaries in list to process".format(len(list_of_dicts))
""" For where the Yaml is formatted as a list a reminder:
All members of a list are lines beginning at the same indentation level
starting with a dash then space (- )
"""
with open('filters.yml', 'w') as yaf:

    yaf.write("{0}{1}".format(chr(45)*3,OSLINESEP))
    # Yaml files begin with triple dash (---) above

    for idx,atom_dict in enumerate(list_of_dicts):

        print "### Filter {0} dictionary processed next:".format(idx)
        dict2yaml = {}

        for dkey,dval in atom_dict.items():
            if dkey not in ['doesNotHaveTheWord','hasTheWord','subject']:
                continue
            try:
                kenc = dkey.encode(KEY_DEFAULT_ENCODE)
                valenc = dval.encode(VALUE_DEFAULT_ENCODE)
                dict2yaml[kenc]=valenc
                #print "{0}: {1}".format(kenc,valenc)
            except UnicodeEncodeError:
                print "### Filter {0} {1} has encoding issue".format(idx,'+'*6)
                print "### Filter {0} {1} has not been output to Yaml".format(idx,'+'*6)

        if len(dict2yaml) > 0:
            yaf.write("# Filter {0:d} having {1}".format(idx,len(dict2yaml)))
            yaf.write(" fields is next:{0}".format(OSLINESEP))
            #
            label2yaml = "filter{0:d}".format(idx)
            nested2yaml = {}
            nested2yaml[label2yaml]=dict2yaml
            """ dump is paired with load whereas safe_dump is paired with safe_load
            Your choice now is safe_dump() with allow_unicode=True and on the receiving
            server a safe_load() 
            """
            yaf.write(yaml.safe_dump(nested2yaml, default_flow_style=False,
                                     allow_unicode=True, width=80, indent=YINDENT))


